# Ollama embedding Demo

This repo reproduces example from
[Olama Embedding models](https://ollama.com/blog/embedding-models) blog post.

## A few notable changes
1. `llama2` model is changed to `gemma:2b` for faster inference on a CPU;
2. Added additional verbose messages to `example.py`;
3. Added Docker for easier development and testing;

# How to use
Clone the repo and run:
```
docker compose up
```

Open second terminal and login to container:
```
docker compose exec app bash
```

Start inference:
```
python3 example.py
```
