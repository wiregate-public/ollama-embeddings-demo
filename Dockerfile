FROM ollama/ollama:0.1.43

WORKDIR /app

RUN ollama serve & sleep 5; ollama pull gemma:2b; ollama pull mxbai-embed-large; \
    echo "kill 'ollama serve' process"; \
    ps -ef | grep 'ollama serve' | grep -v grep | awk '{print $2}' | xargs -r kill -9
RUN apt-get update; apt-get install -y python3-pip
RUN pip install ollama chromadb
